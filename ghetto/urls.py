from django.urls import path
from . import views

urlpatterns = [
	path('',                           views.index,                         name='ghetto index'),
	path('sec/<sid>',                  views.section,                       name='ghetto section'),
	path('thr/<tid>',                  views.thread,                        name='ghetto thread'),
]
