from django.db import models


class User (models.Model):
	uid = models.IntegerField(unique=True)
	display_name = models.CharField(max_length=250)
	email = models.CharField(max_length=250)
	date_joined = models.DateTimeField()


class Section (models.Model):
	sid = models.IntegerField(unique=True)
	display_name = models.CharField(max_length=250)
	display_desc = models.TextField()


class Thread (models.Model):
	tid = models.IntegerField(unique=True)
	section = models.ForeignKey(Section, on_delete=models.CASCADE)
	title = models.TextField()


class Post (models.Model):
	pid = models.IntegerField(unique=True)
	author = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
	thread = models.ForeignKey(Thread, on_delete=models.CASCADE)
	date_created = models.DateTimeField()
	text = models.TextField()
	visible = models.BooleanField()
	likes = models.IntegerField()
	dislikes = models.IntegerField()
