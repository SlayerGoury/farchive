from precise_bbcode.bbcode.tag import BBCodeTag
from precise_bbcode.tag_pool import tag_pool

class QuoteTag(BBCodeTag):
	name = 'quote'
	definition_string = '[quote={TEXT1}]{TEXT2}[/quote]'
	format_string = '<blockquote><span class="quoted">{TEXT1}</span>: {TEXT2}</blockquote>'

	class Options:
		swallow_trailing_newline = True


class BoldTag(BBCodeTag):
	name = 'b'
	definition_string = '[b]{TEXT}[/b]'
	format_string = '<b>{TEXT}</b>'


class ItalicTag(BBCodeTag):
	name = 'i'
	definition_string = '[i]{TEXT}[/i]'
	format_string = '<i>{TEXT}</i>'


class SizeTag(BBCodeTag):
	name = 'size'
	definition_string = '[size={TEXT2}]{TEXT1}[/size]'
	format_string = '<span class="size" data-size=\'{TEXT2}\'>{TEXT1}</span>'


class ColorTag(BBCodeTag):
	name = 'color'
	definition_string = '[color={TEXT2}]{TEXT1}[/color]'
	format_string = '<span class="color" data-color=\'{TEXT2}\'>{TEXT1}</span>'


class URLTag(BBCodeTag):
	name = 'URL'
	definition_string = '[URL={TEXT2}]{TEXT1}[/URL]'
	format_string = '<a href={TEXT2}>{TEXT1}</a>'


tag_pool.register_tag(QuoteTag)
tag_pool.register_tag(BoldTag)
tag_pool.register_tag(ItalicTag)
tag_pool.register_tag(SizeTag)
tag_pool.register_tag(ColorTag)
tag_pool.register_tag(URLTag)
