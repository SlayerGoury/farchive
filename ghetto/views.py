from django.db.models import Count
from django.shortcuts import render
from .models import *


def index (request):
	sections = Section.objects.all().order_by('sid').annotate(threads=Count('thread'))
	return render(request, 'index.html', {'sections': sections}, status=200)


def section (request, sid):
	try:
		section = Section.objects.get(sid=sid)
	except Section.DoesNotExist:
		return render(request, '400.html', {'message': 'Зачем ты сюда ходишь? Чего тебе тут надо? Иди обратно.'}, status=400)
	threads = Thread.objects.filter(section=section).order_by('-tid').annotate(posts=Count('post'))
	return render(request, 'section.html', {'section': section, 'threads': threads}, status=200)


def thread (request, tid):
	try:
		thread = Thread.objects.get(tid=tid)
	except Thread.DoesNotExist:
		return render(request, '400.html', {'message': 'Отказ, совсем отказ!'}, status=400)
	posts = Post.objects.filter(thread=thread, visible=True).order_by('date_created').prefetch_related('author')
	return render(request, 'thread.html', {'thread': thread, 'posts': posts}, status=200)
