from django.apps import AppConfig


class GhettoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ghetto'
